
import random #importer le random
import os

os.system("cls")

#creation de tableau pour les mot
tabFacile = ["chien", "pizza", "porte", "maison"]
tabMoyen = ["fenetre", "ordinateur", "smartphone", "poubelle"]
tabDifficile = ["anticonstitutionnellement", "pluridisciplinaire", "socioconstructivisme", "metamorphose"]
#creation de tableau pour les numero d'essai
tabNubEssai = [1,2,3,4,5,6,7,8,9,10]
#tableau avec lettre pour verifier validiter
tabAlphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
#creation d'un tableau pour garder les proposition joueur
tabProposition =[]
#tab pour recuperer le mot
tabMot=[]
#tab pour voir si les niveua son finis 
tabFacileStatu = ["o","o","o","o"]
tabMoyenStatu = ["o","o","o","o"]
tabDifficileStatu = ["o","o","o","o"]
countFacile = 0
countMoyen = 0
countDifficile = 0
echec = 0
totalCount = 0


#--------------------------------------------------------------------------------------------------------------

#fonction et procédure

#fonction qui teste si le caractère saisi par l'utilisateur est bien 1 2 ou 3 et retourne True ou False
def userCaractVerif(userLvl,tabFacileStatu,tabMoyenStatu,tabDifficile):
    if userLvl == "1" or userLvl == "2" or userLvl == "3":
        if userLvl =="1" and tabFacileStatu[3] == "*":
            print("Le niveau 1 est termineé, veuillez choisire un autre niveau")
            return "lvlFin"
        elif userLvl =="2" and tabMoyenStatu[3] == "*":
            print("Le niveau 2 est termineé, veuillez choisire un autre niveau")
            return "lvlFin"
        elif userLvl =="3" and tabDifficileStatu[3] == "*":
            print("Le niveau 3 est termineé, veuillez choisire un autre niveau")
            return "lvlFin"

        return True
    else :
        return False
#--------------------------------------------------------------------------------------------------------------

#Fonction qui regarde si lvl 1 2 ou 2 et utilise random pour choisir le mot dans le bon tableau , et le retourne ensuite 

def choisirLvl(userLvl):
    #initialise l'indice du tableau grace au random de 0 a 3 , car l'indice max de tout les tableau est 3 : 0,1,2,3 donc 4 mot par tableau
    indice = random.randint(0,3)
    if userLvl == "1":
        motMystere = tabFacile[indice]  #tableau facile
    elif userLvl == "2" :
        motMystere = tabMoyen[indice]   #tableau moyen
    else:
        motMystere = tabDifficile[indice]   #tableau difficile
    #retourn le mot myster 
    return motMystere

#Fonction qui affiche le tableau au fur et a mesure
def affichageTableauJeux(tabPropoRater,tabProposition,tabEchec):
    
    myvar = ""
    for i in range(0,len(tabProposition)):
     myvar = myvar +" "+ tabProposition[i]
    print("Le mot Mystère : ",myvar)
    print("|-------------------------------------------------------------------------------|"+"    "+tabEchec[4]+tabEchec[5]+tabEchec[6])
    print("|                 |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  10   |"+"    "+tabEchec[3]+"    "+tabEchec[7])
    print("|-------------------------------------------------------------------------------|"+"    "+tabEchec[2]+"   "+tabEchec[9]+tabEchec[8]+tabEchec[10])
    print("| "+"Proposition "+"    |"+"\u001b[32m "+tabPropoRater[0]+ "   \u001b[37m"+"|"+"\u001b[32m "+tabPropoRater[1]+"  \u001b[37m"+" |"+"\u001b[32m "+tabPropoRater[2]+"  \u001b[37m"+" |"+"\u001b[32m "+tabPropoRater[3]+"  \u001b[37m"+   " |"+ "\u001b[33;1m "+tabPropoRater[4]+"  \u001b[37m"+ " |"+"\u001b[33;1m "+tabPropoRater[5]+"  \u001b[37m"+" |"+"\u001b[33;1m "+tabPropoRater[6]+"  \u001b[37m"+" |"+"\u001b[31;1m "+tabPropoRater[7]+"  \u001b[37m"+" |"+"\u001b[31;1m "+tabPropoRater[8]+"  \u001b[37m"+" |"+"\u001b[31;1m "+tabPropoRater[9]+"  \u001b[37m"+"   | "+"   "+tabEchec[2]+"   "+tabEchec[11]+" "+tabEchec[12])
    print("|-------------------------------------------------------------------------------|"+"    "+tabEchec[1]+tabEchec[0])

#fonction qui compare le mot mystere
def comparerLeMotMystere(motMystere,propositionUser,afficherProposition):
    
       # affichageTableauJeux(propositionUser

        return False
#compare la lettre au tableau du mot et l'ajoute au tableau mot si la lettre n'est pas bonne virrifie si la letre est dan le tableau des pas bonne sinnol'ajoute au tableau des lettre pas bonne 
def compareLettre(motPropose,tabMot,tabProposition,motMystere,essai,tabEchec,echec):
    #compare la lettre au tableau du mot
    for i in range(0,len(tabMot)):
        motVar1 = ""
        if motPropose == tabMot[i]:
            #l'ajoute au tableau mot
            tabProposition[i] = tabMot[i]
            tabMot[i]="_"
            #met le tableau dans une variable en chaine de caractère
            for x in tabProposition:
                motVar1=motVar1 + x
            #compare si la chaine de caractère du tableau est la meme que le mot a trouver 
            if motVar1 == motMystere:
                print("\u001b[32m","Gagnè ! ","\u001b[37m")
                return "gg",echec # valeur qui me permet de stoper la boucle

            else:
                return "oui",echec# valeur  qui met +1 au compteur de ma boucle
        
           # virrifie si la letre est dan le tableau des pas bonne
    for i in range(0,len(tabPropoRater)):
        if motPropose == tabPropoRater[i]:
            print("\u001b[35m","Vous l'avez deja proposée !!","\u001b[37m")
            return "oui",echec# valleur qui permet de ne rien mettre dans ma boucle
            #regarde si la lettre a deja eter entrée 
        
        
        
    for i in range(0,len(tabPropoRater)):
        if tabPropoRater[i] == " ":
            tabPropoRater[i]=motPropose
            tabEchec,echec = echecBonom(tabEchec,echec)
            #print(tabEchec)
            return "non",echec

#modifie le tableau en cas d'echec et afiche le bonnom
def echecBonom(tabEchec,echec):
    echec = echec+1
    #print(echec)
    if echec == 1:
        tabEchec[0]="__________"
        #print(tabEchec)
        return tabEchec,echec
    elif echec == 2:
        tabEchec[1]="|"
        tabEchec[2]="|"
        tabEchec[3]="|"
        tabEchec[4]="|"
        return tabEchec,echec
    elif echec == 3:
        tabEchec[5]="----"
        return tabEchec,echec

    elif echec == 4:
        tabEchec[6]="|"
        return tabEchec,echec


    elif echec == 5:
        tabEchec[7]="o"
        return tabEchec,echec

    elif echec == 6:
        tabEchec[8]="|"
        return tabEchec,echec

    elif echec == 7:
        tabEchec[9]="/"
        return tabEchec,echec

    elif echec == 8:
        tabEchec[10]="\\"
        return tabEchec,echec

    elif echec == 9:
        tabEchec[11]="/"
        return tabEchec,echec

    else:
        tabEchec[12]="\\"
        return tabEchec,echec
    


#verifier la lettre taper 
def verif2(aVerif,tabAlphabet):
    for i in range(0,len(tabAlphabet)):
        if aVerif == tabAlphabet[i]:
            return True
    else:
        return False

def lvlStatuUp(lvl,tabFacileStatu,tabMoyenStatu,tabDifficileStatu,countFacile,countMoyen,countDifficile):
    if lvl == "1":
        tabFacileStatu[countFacile] = "*"
        countFacile = countFacile +1
        return countFacile,countMoyen,countDifficile
    elif lvl == "2" :
        tabMoyenStatu[countMoyen] = "*"
        countMoyen = countMoyen +1
        return countFacile,countMoyen,countDifficile

    else:
        tabDifficileStatu[countDifficile] = "*"
        countDifficile = countDifficile +1
        return countFacile,countMoyen,countDifficile


        

    

        

        


#///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#Debut du Main du programme
rejouer = "True"
#boucle rejouer
while rejouer == "True":
    tabProposition =[]
#tab pour recuperer le mot
    tabMot=[]
#tab pour recuperer les proposition rater
    tabPropoRater = [" "," "," "," "," "," "," "," "," "," "]
    tabEchec=[" "," "," "," "," "," "," "," "," "," "," "," "," "]

#demande a l'utilisateur de choisire sont niveau 1 2 ou 3
    print("|-------------------------------------------------------------|")
    print("| Le Pendu :")
    print("|-------------------------------------------------------------|")
    print(" Vous avez réussi ",countFacile+countMoyen+countDifficile,"niveaux")
    print("|-------------------------------------------------------------|")
    print("\u001b[32m","choisissez un niveau : \n 1 = Niveau facile     : 5 à 6 lettres",tabFacileStatu[0],tabFacileStatu[1],tabFacileStatu[2],tabFacileStatu[3],"    \u001b[37m")
    print ("\u001b[33;1m","2 = Niveau moyen      : 7 à 10 lettres",tabMoyenStatu[0],tabMoyenStatu[1],tabMoyenStatu[2],tabMoyenStatu[3],"    \u001b[37m") 
    print("\u001b[31;1m","3 = Niveau difficile  : 12 à 26 lettres ",tabDifficileStatu[0],tabDifficileStatu[1],tabDifficileStatu[2],tabDifficileStatu[3],"    \u001b[37m")
    userLvl = str(input("Quel niveau ? "))

#Envoi la réponse de l'utilisateur dans la fonction userCaractVerif pour verifier si le caractère est correcte et retourne True ou False
    verif = userCaractVerif(userLvl,tabFacileStatu,tabMoyenStatu,tabDifficile)
    while verif == "lvlFin":
        userLvl = str(input("Quel niveau ? "))
        verif = userCaractVerif(userLvl)
        

    #Boucle qui dit a l'utilisateur que le niveau est inconnu et lui redemande un niveau tant que la variable verif (ce qu'il a taper ) est Fasle
    while verif == False:
        print("\u001b[35m", "Niveau inconnu : Saisir 1, 2 ou 3","\u001b[37m")
        userLvl = str(input("Quel niveau ? "))
        verif = userCaractVerif(userLvl)
    #while fin ------------------------------------
    #debug verif le debut de la parie
    #print("le jeux commance")
    #Recuperer le mot aleatoirement en fonction du niveau choisis
    motMystere = choisirLvl(userLvl)
    #debug afficher le mot mystere
    #print(motMystere)
    #met le mot mystere dans un tableau
    for i in motMystere:
        tabMot.append(i)
    #cree le tableau des lettre trouvée et met des _ 
    for i in range(0,len(tabMot)):
        tabProposition.append("_")
    #affichageTableauJeux1()
    essai = 0
    motPropose = ("")

    while essai < 10:
        affichageTableauJeux(tabPropoRater,tabProposition,tabEchec)
        
        motPropose = input("Votre proposition "+ str(essai+1) + ": ")
        verific = verif2(motPropose,tabAlphabet)

        while verific == False:
            print("\u001b[35m","Ce n'est pas une lettre !!","\u001b[37m")
            motPropose = input("poposer une lettre ")
            verific = verif2(motPropose,tabAlphabet)

        retourFunc,echec = compareLettre(motPropose,tabMot,tabProposition,motMystere,essai,tabEchec,echec)

        #for i in range(0,len(tabProposition)):
            #print(tabProposition[i])
        #os.system("cls")
        print(motMystere)
        #print(retourFunc)
        if retourFunc == "non":
            essai = essai+1
            if essai == 10:
                os.system("cls")
                print("**********************************************************") 
                print("\u001b[31;1m","C'est perdu !  Le mot était : ", motMystere,"\u001b[37m")

        elif retourFunc == "gg":
            essai = 10
            countFacile,countMoyen,countDifficile= lvlStatuUp(userLvl,tabFacileStatu,tabMoyenStatu,tabDifficileStatu,countFacile,countMoyen,countDifficile)

        
        
        
    #while fin ---------------------------------------------------------------
    caractSaisibool = False
    print("**********************************************************")
    while caractSaisibool == False:

        rejouer =input("voulez-vous rejouer True/False ?")
        if rejouer == "True" :
            caractSaisibool = True
            rejouer = "True"
        elif  rejouer == "False" :
            caractSaisibool = True 
            rejouer = "False"
        else: 
            print("\u001b[35m","Réponse incorrecte : True ou False uniquement","\u001b[37m")
print("Au revoir")   











    
    

